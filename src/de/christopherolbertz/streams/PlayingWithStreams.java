package de.christopherolbertz.streams;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Enthaelt einige Methoden, welche die Arbeit mit den Java8-Streams zeigen.
 * HINWEIS: Die Aufrufe der beiden Methoden fuer die unendlichen Streams habe ich auskommentiert.
 * Wenn Sie diese testen moechten, kommentieren Sie bitte immer nur eine der beiden
 * Methoden ein, denn schliesslich laeuft der erste der beiden Streams ja unendlich
 * lange.
 * @author Christopher Olbertz
 *
 */
public class PlayingWithStreams {
	public void collectionBeforeJava8() {
		System.out.println("Collections vor Java 8");
		List<String> days = Arrays.asList("Montag", "Dienstag", 
				"Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag");
		
		for (int i = 0; i < days.size(); i++) {
		    String day = days.get(i);
		    if (day.length() > 7) {
		        System.out.println(day);
		    }
		}
				
		for (String day: days) {
		    if (day.length() > 7) {
		        System.out.println(day);
		    }
		}

		Iterator<String> dayIterator = days.iterator();
		while(dayIterator.hasNext()) {
			String day = dayIterator.next();
		    if (day.length() > 7) {
		        System.out.println(dayIterator.next());
		    }
		}
	}
	
	public void creatingStreamsExample() {
		System.out.println("Erzeugen von Streams");
		Stream.of("AA", "BB", "CC", "DD").forEach(myString -> System.out.println(myString));
		List<Integer> myList = Arrays.asList(1,2,3,4,5,6);
		myList.stream().forEach(myNumber -> System.out.println(myNumber));
		
		IntStream.of(1,2,3,4,5,6).
				  average().
				  ifPresent(System.out::println);
		
		Stream.of("x", "a", "z", "d", "e", "b", "d").
		      sorted().
		      forEach(myString -> System.out.println(myString));

		Stream.of("x", "a", "z", "d", "e", "b", "d").
	      sorted((string1, string2) -> string2.compareTo(string1)).
	      forEach(myString -> System.out.println(myString));
	}
	
	public void forEachExample() {
		System.out.println("Demonstriert forEach()");
		List<String> days = Arrays.asList("Montag", "Dienstag", 
				"Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag");
		days.stream().forEach(day -> {
		    if (day.length() > 7) {
		         System.out.println(day);
		    }
		});
	}
	
	public void filterExample() {
		System.out.println("Demonstriert filter()");
		List<String> days = Arrays.asList("Montag", "Dienstag", 
				"Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag");
		
		days.stream()
	    .filter(day -> day.length() > 7)
	    .forEach(day -> System.out.println(day));
	}
	
	public void intStreamExample() {
		System.out.println("Demonstriert IntStream");
		IntStream.of(1,2,3,4,5,6).
		  average().
		  ifPresent(System.out::println);
	}
	
	public void mapExample() {
		System.out.println("Demonstriert map()");
		List<String> days = Arrays.asList("Montag", "Dienstag", 
				"Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag");
		
		days.stream()
	    .filter(day -> day.length() > 7)
	    .map(day -> day.toUpperCase())
	    .forEach(day -> System.out.println(day));
	}
	
	public void showProcessingOrder() {
		System.out.println("Demonstriert die Verarbeitungsreihenfolge");
		Stream.of("A", "B", "C", "D")
	    .filter(myString -> {
	        System.out.println("filter: " + myString);
		  return true;
	    })
	    .forEach(myString -> System.out.println("forEach: " + myString));
	}
	
	public void showProcessingOrderWithAnyMatch() {
		System.out.println("Demonstriert die Verarbeitungsreihenfolge, wenn mit anyMatch() "
				+ "nach dem zweiten Element abgebrochen wird");
		Stream.of("AA", "BB", "CC", "DD")
	    .map(myString -> {
	        System.out.println("map: " + myString);
	        return myString.toLowerCase();
	    })
	    .anyMatch(s -> {
	        System.out.println("anyMatch: " + s);
		  return s.contains("b");
	    });
	}
	
	public void sortedExamples() {
		System.out.println("Aufsteigend sortiert");
		Stream.of("x", "a", "z", "d", "e", "b", "d").
	       sorted().
	       forEach(myString -> System.out.println(myString));

		System.out.println("Absteigend sortiert");
		Stream.of("x", "a", "z", "d", "e", "b", "d").
	       sorted((string1, string2) -> string2.compareTo(string1)).
	       forEach(myString -> System.out.println(myString));
	}
	
	public void fluentApiExamples() {
		System.out.println("Demonstriert die FluentAPI");
		List<String> days = Arrays.asList("Montag", "Dienstag", 
				"Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag");
		
		days.stream()
	    .filter(day -> day.length() > 7)
	    .count();
			
		List<String> strings = Arrays.asList("ASDFGHASFD", "A", "B", "AAAA", "A",
				"AAAA", "XXXX", "XXXX", "BBBBB", "X", "DDDD", "YYYYY");
	
		strings.stream()
		    .filter(string -> string.length() >= 3)
		    .filter(string -> string.length() < 8)
		    .distinct()
		    .limit(4)
		    .map(string -> string.toLowerCase())
		    .forEach(string -> System.out.println(string));
	}
	
	public void parallelStreamExamples() {
		List<String> strings = Arrays.asList("ASDFGHASFD", "A", "B", "AAAA", "A",
				"AAAA", "XXXX", "XXXX", "BBBBB", "X", "DDDD", "YYYYY");
		
		System.out.println("Erstes Beispiel fuer parallele Streams");
		int charCount = strings.parallelStream()
			    .filter(string -> string.length() >= 3)
			    .filter(string -> string.length() < 8)			
			    .map(String::length)
			    .reduce(0, (i,j) -> i+j);
		System.out.println("Ergebnis: " + charCount);
		
		System.out.println("Zweites Beispiel fuer parallele Streams");
		charCount = strings.parallelStream()
			    .filter(string -> {
			        System.out.format("filter1: %s [%s]\n",
				      string, Thread.currentThread().getName());
				  return string.length() >= 3;
			    })
			    .filter(string -> {
			        System.out.format("filter2: %s [%s]\n",
			            string, Thread.currentThread().getName());
				  return string.length() < 8;
			    })
			    .map(string -> {
			         System.out.format("map: %s [%s]\n",
			             string, Thread.currentThread().getName());
			         return string.length();
			     })
			     .reduce(0, (i,j) -> {
			          System.out.format("reduce: [%s]\n",
			               Thread.currentThread().getName());
			          return i+j;
			});
		System.out.println("Ergebnis: " + charCount);
	}
	
	public void collectorExamples() {
		List<String> strings = Arrays.asList("ASDFGHASFD", "A", "B", "AAAA", "A",
				"AAAA", "XXXX", "XXXX", "BBBBB", "X", "DDDD", "YYYYY");
		
		System.out.println("Erstes Beispiel fuer Collector");
		List<Integer> charCountList = strings.parallelStream()
			    .filter(string -> string.length() >= 3)
			    .filter(string -> string.length() < 8)		
			    .map(String::length)
			    .collect(Collectors.toList());
		
		System.out.println("Ergebnis mit den Laengen der Strings: ");
		charCountList.stream().forEach(sizeOfString -> {
			System.out.println(sizeOfString);
		});
		
		
		System.out.println("Zweites Beispiel fuer Collector");
		int charCount = strings.parallelStream()
		    .filter(string -> string.length() >= 3)
		    .filter(string -> string.length() < 8)			
		    .collect(Collectors.summingInt(String::length));
		System.out.println("Ergebnis mit der Gesamtlaenge aller Strings: " + charCount);

		System.out.println("Drittes Beispiel fuer Collector");
		Map<Integer, List<String>> groups = strings.stream()
		    .filter(string -> string.length() >= 3)
		    .filter(string -> string.length() < 8)
		    .collect(Collectors.groupingBy(String::length));
		
		groups.entrySet().forEach(element -> {
			int key = element.getKey();
			System.out.println("Elemente mit Laenge: " + key);
			List<String> stringsWithKey = groups.get(key);
			stringsWithKey.forEach(theString -> System.out.println(theString));
		});
	}
	
	public void infiniteStreamsExample1() {
		System.out.println("Unendlicher Streams mit Zufallszahlen");
		Stream<Double> randomDoubleStream = Stream.generate (Math::random);
		randomDoubleStream.forEach(myValue -> System.out.println(myValue));
	}
	
	public void infiniteStreamsExample2() {
		System.out.println("Unendlicher Streams mit Erhoehung um zwei");
		Stream<Integer> squareIntegerStream = Stream.iterate(2, i -> i + 2);
		squareIntegerStream.forEach(myValue -> System.out.println(myValue));
	}
}
