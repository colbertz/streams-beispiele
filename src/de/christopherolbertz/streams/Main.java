package de.christopherolbertz.streams;

public class Main {
	public static void main(String [] args) {
		PlayingWithStreams playingWithStreams = new PlayingWithStreams();
		playingWithStreams.collectionBeforeJava8();
		playingWithStreams.creatingStreamsExample();
		playingWithStreams.forEachExample();
		playingWithStreams.filterExample();
		playingWithStreams.intStreamExample();
		playingWithStreams.mapExample();
		playingWithStreams.showProcessingOrder();
		playingWithStreams.showProcessingOrderWithAnyMatch();
		playingWithStreams.sortedExamples();
		playingWithStreams.fluentApiExamples();
		playingWithStreams.parallelStreamExamples();
		playingWithStreams.collectorExamples();
		//playingWithStreams.infiniteStreamsExample1();
		//playingWithStreams.infiniteStreamsExample2();
	}
}
